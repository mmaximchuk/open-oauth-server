package ua.maximchuk.oauth.rest;

import org.apache.log4j.Logger;

import javax.ws.rs.core.Response;

/**
 * @author Maxim Maximchuk
 *         date 10.12.2014.
 */
public class BaseController {
    private Logger log = Logger.getLogger(getClass());

    protected Response internalError(Exception e) {
        e.printStackTrace();
        log.error(e);
        return Response.serverError().build();
    }

}
