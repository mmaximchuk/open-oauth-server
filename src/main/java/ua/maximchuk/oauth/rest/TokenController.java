package ua.maximchuk.oauth.rest;

import ua.maximchuk.oauth.datamodel.dao.AuthInfoDao;
import ua.maximchuk.oauth.datamodel.dao.TokenDao;
import ua.maximchuk.oauth.datamodel.entity.AuthInfoEntity;
import ua.maximchuk.oauth.datamodel.entity.TokenEntity;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

/**
 * @author Maxim Maximchuk
 *         date 16.03.15.
 */
@Path("session")
public class TokenController extends BaseController {

    @GET
    @Path("check")
    @Produces("application/json")
    public Response checkAccessToken(@QueryParam("token") String token) {
        Response response;
        if (token != null) {
            try {
                AuthInfoDao dao = new AuthInfoDao();
                AuthInfoEntity authInfo = dao.getAuthInfoByAccessToken(token);

                if (authInfo != null) {
                    response = Response.ok(authInfo.toJSON()).build();
                } else {
                    response = Response.status(Response.Status.UNAUTHORIZED).build();
                }
            } catch (Exception e) {
                return internalError(e);
            }
        } else {
            response = Response.status(Response.Status.BAD_REQUEST).build();
        }
        return response;
    }

    @GET
    @Path("invalidate")
    @Consumes("text/plain")
    public Response invalidateToken(@QueryParam("token")String token) {
        Response response;
        if (token != null) {
            try {
                TokenDao dao = new TokenDao();
                TokenEntity tokenEntity = dao.findRefreshToken(token);
                if (tokenEntity != null) {
                    dao.invalidateRefreshToken(tokenEntity);
                    response = Response.ok().build();
                } else {
                    response = Response.status(Response.Status.NOT_MODIFIED).build();
                }
            } catch (Exception e) {
                return internalError(e);
            }
        } else {
            response = badRequest();
        }
        return response;
    }

    private Response badRequest() {
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

}
