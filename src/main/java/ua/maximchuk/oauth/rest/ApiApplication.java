package ua.maximchuk.oauth.rest;

import com.maximchuk.json.rest.provider.JSONArrayProvider;
import com.maximchuk.json.rest.provider.JSONObjectProvider;
import com.maximchuk.json.rest.provider.JsonDtoListProvider;
import com.maximchuk.json.rest.provider.JsonDtoProvider;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Maxim Maximchuk
 *         date 18.12.14.
 */
@ApplicationPath("api")
public class ApiApplication extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        return new HashSet<>(Arrays.asList(
                ApplicationController.class,
                TokenController.class,

                JSONObjectProvider.class,
                JSONArrayProvider.class,
                JsonDtoProvider.class,
                JsonDtoListProvider.class
        ));
    }
}
