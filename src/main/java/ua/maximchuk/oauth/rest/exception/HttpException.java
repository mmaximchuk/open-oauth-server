package ua.maximchuk.oauth.rest.exception;

import javax.ws.rs.core.Response;

/**
 * @author Maxim Maximchuk
 *         date 18.12.14.
 */
public class HttpException extends Exception {

    private Response.Status status;
    private String message;

    public HttpException(Response.Status status) {
        this.status = status;
    }

    public HttpException(Response.Status status, Throwable throwable) {
        super(throwable);
        this.status = status;
    }

    public HttpException(Response.Status status, String message) {
        this(status);
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    protected void setMessage(String message) {
        this.message = message;
    }

    public Response createResponse() {
        return Response.status(status).entity(getMessage()).build();
    }
}
