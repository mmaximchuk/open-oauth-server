package ua.maximchuk.oauth.rest.exception;

import javax.ws.rs.core.Response;

/**
* @author Maxim Maximchuk
*         date 19.12.2014.
*/
public class AuthAppException extends HttpException {
    public static final int INCORRECT_CLIENT_ID = 0;
    public static final int INCORRECT_CLIENT_SECRET = 1;

    private int code;

    public AuthAppException(int code) {
        super(Response.Status.UNAUTHORIZED);
        this.code = code;
        switch (code) {
            case INCORRECT_CLIENT_ID: setMessage("incorrect_client_id"); break;
            case INCORRECT_CLIENT_SECRET: setMessage("incorrect_client_secret"); break;
        }
    }

    public int getCode() {
        return code;
    }

}
