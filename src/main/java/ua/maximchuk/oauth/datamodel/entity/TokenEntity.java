package ua.maximchuk.oauth.datamodel.entity;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * @author Maxim Maximchuk
 *         date 03.10.2014.
 */
@DatabaseTable(tableName = "token")
public class TokenEntity implements IdEntity {

    public static final String TOKEN_FIELD = "token";
    public static final String TYPE_FIELD = "type";
    public static final String EXPIRES_FIELD = "expires";
    public static final String CREATE_DATE_FIELD = "create_date";

    @DatabaseField(generatedId = true)
    private Long id;

    @DatabaseField(columnName = TOKEN_FIELD, canBeNull = false)
    private String token;

    @DatabaseField(columnName = TYPE_FIELD, canBeNull = false)
    private Type type;

    @DatabaseField(columnName = CREATE_DATE_FIELD, dataType = DataType.DATE, canBeNull = false)
    private Date createDate;

    @DatabaseField(columnName = EXPIRES_FIELD)
    private Integer expires;

    @DatabaseField(foreign = true)
    private AuthInfoEntity authInfo;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Integer getExpires() {
        return expires;
    }

    public void setExpires(Integer expires) {
        this.expires = expires;
    }

    public AuthInfoEntity getAuthInfo() {
        return authInfo;
    }

    public void setAuthInfo(AuthInfoEntity authInfo) {
        this.authInfo = authInfo;
    }

    public enum Type {
        ACCESS, REFRESH;
    }
}
