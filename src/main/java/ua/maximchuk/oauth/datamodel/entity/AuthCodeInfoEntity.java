package ua.maximchuk.oauth.datamodel.entity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author Maxim Maximchuk
 *         date 19.12.2014.
 */
@DatabaseTable(tableName = "auth_code_info")
public class AuthCodeInfoEntity implements IdEntity {

    public static final String AUTH_CODE_FIELD = "auth_code";
    public static final String SCOPES_FIELD = "scopes";
    public static final String USERNAME_FIELD = "username";

    @DatabaseField(generatedId = true)
    private Long id;

    @DatabaseField(columnName = USERNAME_FIELD)
    private String username;

    @DatabaseField(foreign = true, canBeNull = false, foreignAutoRefresh = true)
    private ApplicationEntity application;

    @DatabaseField(columnName = AUTH_CODE_FIELD)
    private String authCode;

    @DatabaseField(columnName = SCOPES_FIELD)
    private String scopes;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public ApplicationEntity getApplication() {
        return application;
    }

    public void setApplication(ApplicationEntity application) {
        this.application = application;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public String getScopes() {
        return scopes;
    }

    public void setScopes(String scopes) {
        this.scopes = scopes;
    }
}
