package ua.maximchuk.oauth.datamodel.entity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author Maxim Maximchuk
 *         date 03.10.2014.
 */
@DatabaseTable(tableName = "root_token")
public class RootTokenEntity {

    public static final String TOKEN_FIELD = "token";

    @DatabaseField(columnName = RootTokenEntity.TOKEN_FIELD, id = true)
    private String token;

    public RootTokenEntity() {
        super();
    }

    public RootTokenEntity(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
