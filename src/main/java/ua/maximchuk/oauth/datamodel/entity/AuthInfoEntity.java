package ua.maximchuk.oauth.datamodel.entity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.maximchuk.json.JsonDTO;
import com.maximchuk.json.annotation.JsonIgnore;
import com.maximchuk.json.annotation.JsonParam;
import org.apache.oltu.oauth2.as.request.AbstractOAuthTokenRequest;

/**
 * @author Maxim Maximchuk
 *         date 03.10.2014.
 */
@DatabaseTable(tableName = "auth_info")
public class AuthInfoEntity extends JsonDTO implements IdEntity {

    public static final String CLIENT_ID_FIELD = "client_id";
    public static final String USERNAME_FIELD = "username";
    public static final String SCOPES_FIELD = "scopes";

    @JsonIgnore
    @DatabaseField(generatedId = true)
    private Long id;

    @JsonParam(name = CLIENT_ID_FIELD)
    @DatabaseField(columnName = CLIENT_ID_FIELD, canBeNull = false)
    private String clientId;

    @DatabaseField(columnName = USERNAME_FIELD, canBeNull = false)
    private String username;

    @DatabaseField(columnName = SCOPES_FIELD)
    private String scopes;

    public AuthInfoEntity() {
    }

    public AuthInfoEntity(AuthCodeInfoEntity authCodeInfoEntity) {
        setClientId(authCodeInfoEntity.getApplication().getClientId());
        setScopes(authCodeInfoEntity.getScopes());
        setUsername(authCodeInfoEntity.getUsername());
    }

    public AuthInfoEntity(AbstractOAuthTokenRequest oauthRequest) {
        this.setClientId(oauthRequest.getClientId());
        StringBuilder scopesBuilder = new StringBuilder();
        for (String scope: oauthRequest.getScopes()) {
            scopesBuilder.append(scope).append(" ");
        }
        setScopes(scopesBuilder.toString().trim());
        setUsername(oauthRequest.getUsername());
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getScopes() {
        return scopes;
    }

    public void setScopes(String scopes) {
        this.scopes = scopes;
    }

}
