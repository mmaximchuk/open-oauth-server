package ua.maximchuk.oauth.datamodel.entity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.maximchuk.json.JsonDTO;
import com.maximchuk.json.annotation.JsonIgnore;
import com.maximchuk.json.exception.JsonException;
import org.json.JSONObject;

/**
 * @author Maxim Maximchuk
 *         date 03.10.2014.
 */
@DatabaseTable(tableName = "user")
public class UserEntity extends JsonDTO implements IdEntity {

    public static final String USERNAME_FIELD = "username";
    public static final String PASSWORD_FIELD = "password";

    @JsonIgnore
    @DatabaseField(generatedId = true)
    private Long id;

    @DatabaseField(columnName = USERNAME_FIELD, unique = true, canBeNull = false)
    private String username;

    @DatabaseField(columnName = UserEntity.PASSWORD_FIELD, canBeNull = false)
    private String password;

    public UserEntity() {
    }

    public UserEntity(JSONObject json) throws JsonException {
        super(json);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
