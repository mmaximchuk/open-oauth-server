package ua.maximchuk.oauth.datamodel.dao;

import ua.maximchuk.oauth.datamodel.entity.RootTokenEntity;

import java.sql.SQLException;

/**
 * @author Maxim Maximchuk
 *         date 03.10.2014.
 */
public class RootTokenDao extends BaseDao<RootTokenEntity, String> {

    public RootTokenDao() throws SQLException {
        super(RootTokenEntity.class);
    }

    public boolean checkToken(String token) throws SQLException {
        return queryBuilder().where().eq(RootTokenEntity.TOKEN_FIELD, token).queryForFirst() != null;
    }

}
