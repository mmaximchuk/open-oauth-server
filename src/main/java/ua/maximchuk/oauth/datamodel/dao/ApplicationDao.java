package ua.maximchuk.oauth.datamodel.dao;

import com.j256.ormlite.stmt.QueryBuilder;
import org.apache.oltu.oauth2.as.issuer.MD5Generator;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import ua.maximchuk.oauth.datamodel.entity.ApplicationEntity;
import ua.maximchuk.oauth.rest.exception.AuthAppException;

import java.sql.SQLException;
import java.util.Date;

/**
 * @author Maxim Maximchuk
 *         date 03.10.2014.
 */
public class ApplicationDao extends BaseDao<ApplicationEntity, Long> {

    public ApplicationDao() throws SQLException {
        super(ApplicationEntity.class);
    }

    public ApplicationEntity registerApplication(String name, String redirectUri) throws OAuthSystemException, SQLException {
        String pref = Long.toString(new Date().getTime() / 1000);

        ApplicationEntity application = new ApplicationEntity();
        application.setClientId(pref + "@" + name);
        application.setClientSecret(new MD5Generator().generateValue());
        application.setRedirectUri(redirectUri);
        return createIfNotExists(application);
    }

    public ApplicationEntity findAppByClientId(String clientId) throws SQLException {
        QueryBuilder<ApplicationEntity, Long> queryBuilder = queryBuilder();
        return queryBuilder.where().eq(ApplicationEntity.CLIENT_ID_FIELD, clientId).queryForFirst();
    }

    public void checkApplication(String clientId, String clientSecret) throws AuthAppException, SQLException {
        ApplicationEntity app = findAppByClientId(clientId);
        if (app == null) {
            throw new AuthAppException(AuthAppException.INCORRECT_CLIENT_ID);
        }
        if (!app.getClientSecret().equals(clientSecret)) {
            throw new AuthAppException(AuthAppException.INCORRECT_CLIENT_SECRET);
        }
    }

}
