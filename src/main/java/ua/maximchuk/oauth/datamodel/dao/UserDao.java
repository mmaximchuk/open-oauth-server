package ua.maximchuk.oauth.datamodel.dao;

import org.apache.oltu.oauth2.as.issuer.MD5Generator;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import ua.maximchuk.oauth.datamodel.entity.UserEntity;

import java.sql.SQLException;

/**
 * @author Maxim Maximchuk
 *         date 06.10.2014.
 */
public class UserDao extends BaseDao<UserEntity, Long> {

    public UserDao() throws SQLException {
        super(UserEntity.class);
    }

    public UserEntity findUser(String username) throws SQLException {
        return queryBuilder().where().eq(UserEntity.USERNAME_FIELD, username).queryForFirst();
    }

    public UserEntity findUser(String username, String password) throws SQLException {
        return queryBuilder().where().eq(UserEntity.USERNAME_FIELD, username)
                .and().eq(UserEntity.PASSWORD_FIELD, passwordHash(password))
                .queryForFirst();
    }

    public UserEntity register(String username, String password) throws SQLException {
        UserEntity user = new UserEntity();
        user.setUsername(username);
        user.setPassword(passwordHash(password));
        return createIfNotExists(user);
    }

    private String passwordHash(String password) {
        String hash = null;
        try {
            MD5Generator md5Generator = new MD5Generator();
            hash = md5Generator.generateValue(password);
        } catch (OAuthSystemException e) {
            e.printStackTrace();
        }
        return hash;
    }

}
