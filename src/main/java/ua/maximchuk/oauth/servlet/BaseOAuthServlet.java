package ua.maximchuk.oauth.servlet;

import org.apache.oltu.oauth2.as.request.OAuthRequest;
import org.apache.oltu.oauth2.as.response.OAuthASResponse;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.OAuthResponse;
import org.json.JSONObject;
import ua.maximchuk.oauth.datamodel.dao.ApplicationDao;
import ua.maximchuk.oauth.datamodel.entity.ApplicationEntity;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

/**
 * @author Maxim Maximchuk
 *         date 11.10.2014.
 */
public class BaseOAuthServlet extends HttpServlet {

    protected ApplicationEntity app;

    protected void validateRequest(OAuthRequest oauthRequest) throws SQLException, OAuthProblemException {
        ApplicationDao dao = new ApplicationDao();
        app = dao.findAppByClientId(oauthRequest.getClientId());
        if (oauthRequest.getRedirectURI() == null) {
            error("redirect_uri_is_missed");
        }
        if (app == null) {
            error("incorrect_client_id");
        }
        if (!app.getRedirectUri().equals(oauthRequest.getRedirectURI())) {
            error("incorrect_redirect_uri");
        }
    }

    protected void sendBodyResponse(HttpServletResponse response, OAuthResponse oauthResponse) throws IOException {
        sendResponse(response, oauthResponse.getResponseStatus(), oauthResponse.getBody());
    }

    protected void sendBodyResponse(HttpServletResponse response, JSONObject jsonObject) throws IOException {
        sendResponse(response, HttpServletResponse.SC_OK, jsonObject.toString());
    }

    protected void sendResponse(HttpServletResponse response, int status, String message) throws IOException {
        response.setStatus(status);
        PrintWriter pw = response.getWriter();
        pw.print(message);
        pw.flush();
        pw.close();
    }

    protected void sendQueryError(HttpServletResponse response, OAuthProblemException e, String location) throws IOException, OAuthSystemException {
        final OAuthResponse resp = OAuthASResponse
                .errorResponse(HttpServletResponse.SC_FOUND)
                .error(e)
                .location(location)
                .buildQueryMessage();

        response.sendRedirect(resp.getLocationUri());
    }

    protected void sendBodyError(HttpServletResponse response, Exception e) throws IOException {
        response.setStatus(HttpServletResponse.SC_FOUND);
        PrintWriter pw = response.getWriter();
        pw.print(e.getMessage());
        pw.flush();
        pw.close();
    }

    protected void sendInternalError(HttpServletResponse response) throws IOException {
        response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
    }

    protected void error(String msg) throws OAuthProblemException {
        throw OAuthProblemException.error(msg);
    }
}
