package ua.maximchuk.oauth.servlet;

import ua.maximchuk.oauth.datamodel.dao.UserDao;
import ua.maximchuk.oauth.datamodel.entity.UserEntity;

/**
 * @author Maxim Maximchuk
 *         date 02.10.2014.
 */
public class EndUserAuthorizationServlet extends AbstractEndUserOAuthServlet {

    @Override
    protected UserEntity getUser(String username, String password) throws Exception {
        UserDao userDao = new UserDao();
        return userDao.findUser(username, password);
    }
}
