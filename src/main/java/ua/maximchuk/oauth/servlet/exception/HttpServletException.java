package ua.maximchuk.oauth.servlet.exception;

/**
 * @author Maxim Maximchuk
 *         date 23.12.14.
 */
public class HttpServletException extends Exception {

    private int status;

    public HttpServletException(String message, int status) {
        super(message);
        this.status = status;
    }

    public int getStatus() {
        return status;
    }
}
