package ua.maximchuk.oauth.servlet;

import org.apache.oltu.oauth2.as.issuer.OAuthIssuer;
import org.apache.oltu.oauth2.as.request.OAuthRequest;
import org.apache.oltu.oauth2.as.request.OAuthUnauthenticatedTokenRequest;
import org.apache.oltu.oauth2.as.response.OAuthASResponse;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.OAuthResponse;
import org.apache.oltu.oauth2.common.message.types.GrantType;
import ua.maximchuk.oauth.OAuthStoreIssuer;
import ua.maximchuk.oauth.datamodel.dao.AuthCodeInfoDao;
import ua.maximchuk.oauth.datamodel.dao.AuthInfoDao;
import ua.maximchuk.oauth.datamodel.dao.TokenDao;
import ua.maximchuk.oauth.datamodel.dao.UserDao;
import ua.maximchuk.oauth.datamodel.entity.AuthCodeInfoEntity;
import ua.maximchuk.oauth.datamodel.entity.AuthInfoEntity;
import ua.maximchuk.oauth.datamodel.entity.TokenEntity;
import ua.maximchuk.oauth.datamodel.entity.UserEntity;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * @author Maxim Maximchuk
 *         date 02.10.2014.
 */
public class TokenEndpointServlet extends BaseOAuthServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // CORS support
            response.addHeader("Access-Control-Allow-Origin", "*");
            response.addHeader("Access-Control-Allow-Credentials", "true");
            response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
            response.addHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");


            OAuthUnauthenticatedTokenRequest oauthRequest = new OAuthUnauthenticatedTokenRequest(request);
            String redirectUri = oauthRequest.getRedirectURI();
            String accessToken;
            String refreshToken = null;

            AuthInfoDao dao = new AuthInfoDao();
            AuthInfoEntity authInfo = null;
            try {
                if (oauthRequest.getGrantType().equals(GrantType.AUTHORIZATION_CODE.toString())) {
                    validateAuthorizationCode(oauthRequest);

                    AuthCodeInfoDao codeDao = new AuthCodeInfoDao();
                    AuthCodeInfoEntity authCodeInfo = codeDao.getAuthInfoByCode(oauthRequest.getCode());
                    authInfo = dao.create(authCodeInfo);
                    codeDao.delete(authCodeInfo);
                }
            } catch (OAuthProblemException e) {
                sendQueryError(response, e, redirectUri);
            }

            if (oauthRequest.getGrantType().equals(GrantType.PASSWORD.toString())) {
                validateClientSecret(oauthRequest);
                checkUser(oauthRequest);
                authInfo = dao.create(oauthRequest);
            }
            if (oauthRequest.getGrantType().equals(GrantType.REFRESH_TOKEN.toString())) {
                validateRefreshToken(oauthRequest);
                authInfo = dao.getAuthInfoByRefreshToken(oauthRequest.getRefreshToken());
                refreshToken = oauthRequest.getRefreshToken();
            }

            if (authInfo == null) {
                throw OAuthProblemException.error("unsupported grant type");
            }

            OAuthIssuer issuer = new OAuthStoreIssuer(authInfo);
            accessToken = issuer.accessToken();
            refreshToken = refreshToken == null ? issuer.refreshToken() : refreshToken;

            OAuthResponse oauthResponse = OAuthASResponse.tokenResponse(HttpServletResponse.SC_OK)
                    .setAccessToken(accessToken)
                    .setRefreshToken(refreshToken)
                    .location(redirectUri)
                    .setExpiresIn(String.valueOf(OAuthStoreIssuer.EXPIRES))
                    .buildJSONMessage();

            sendBodyResponse(response, oauthResponse);
        } catch (OAuthProblemException e) {
            sendBodyError(response, e);
        } catch (OAuthSystemException | SQLException e) {
            e.printStackTrace();
            sendInternalError(response);
        }
    }

    @Override
    protected void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        String requestUri = httpServletRequest.getRequestURI();
        String servletPath = httpServletRequest.getServletPath();
        String action = requestUri.substring(requestUri.indexOf(servletPath) + servletPath.length() + 1);

        try {
            if (action.equals("invalidate")) {
                String token = httpServletRequest.getParameter("token");
                if (invalidateRefreshToken(token)) {
                    sendResponse(httpServletResponse, HttpServletResponse.SC_OK, "token invalidated");
                } else {
                    sendResponse(httpServletResponse, HttpServletResponse.SC_GONE, "token not found");
                }
            }
        } catch (Exception e){
            sendBodyError(httpServletResponse, e);
        }
    }

    protected void validateClientSecret(OAuthRequest oauthRequest) throws SQLException, OAuthProblemException {
        validateRequest(oauthRequest);
        if (!app.getClientSecret().equals(oauthRequest.getClientSecret())) {
            error("incorrect_client_secret");
        }

    }

    private void validateAuthorizationCode(OAuthUnauthenticatedTokenRequest oauthRequest) throws OAuthProblemException, SQLException {
        validateRequest(oauthRequest);
        String code = oauthRequest.getCode();
        if (code == null) {
            error("code_is_missed");
        }
        AuthCodeInfoEntity authInfo = new AuthCodeInfoDao().getAuthInfoByCode(code);
        if (authInfo == null || !authInfo.getApplication().getClientId().equals(oauthRequest.getClientId())) {
            error("incorrect_code");
        } else if (authInfo.getUsername() == null) {
            error("code_is_not_authorized_by_user");
        }
    }

    private void checkUser(OAuthUnauthenticatedTokenRequest oauthRequest) throws SQLException, OAuthProblemException {
        UserDao userDao = new UserDao();
        if (oauthRequest.getUsername() == null) {
            error("username_is_not_specified");
        }
        if (oauthRequest.getPassword() == null) {
            error("password_is_not_specified");
        }
        UserEntity user = userDao.findUser(oauthRequest.getUsername(), oauthRequest.getPassword());
        if (user == null) {
            error("incorrect username or password");
        }
    }

    private void validateRefreshToken(OAuthUnauthenticatedTokenRequest oauthRequest) throws SQLException, OAuthProblemException {
        TokenDao tokenDao = new TokenDao();
        TokenEntity refreshTokenEntity = tokenDao.findRefreshToken(oauthRequest.getRefreshToken());
        if (refreshTokenEntity == null) {
            error("incorrect_refresh_token");
        }
        TokenEntity accessTokenEntity = new TokenDao().findAccessTokenByRefreshToken(refreshTokenEntity);
        if (accessTokenEntity != null) {
            tokenDao.delete(accessTokenEntity);
        }
    }

    private boolean invalidateRefreshToken(String token) throws SQLException {
        TokenDao dao = new TokenDao();
        TokenEntity tokenEntity = dao.findRefreshToken(token);
        if (tokenEntity != null) {
            dao.invalidateRefreshToken(tokenEntity);
            return true;
        } else {
            return false;
        }
    }

}
